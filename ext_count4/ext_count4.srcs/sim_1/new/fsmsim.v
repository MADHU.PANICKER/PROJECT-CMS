`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.02.2019 18:17:41
// Design Name: 
// Module Name: fsmsim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fsmsim();

reg clk;
reg start;
reg stop;
//reg transmit;


//wire [6:0] seg7seg; 
//wire [3:0] seg7an;
wire[15:0] latch;
wire TxD;
wire TxD_debug;
initial begin
clk=0;
#1;start=0;
stop=0;
//transmit=0;
#0.002; start=1;
#0.002; start=0;
#0.001; stop=1;
#0.002; stop=0;


#1000000000;
$finish;
end

always begin
clk=~clk;
#0.001;
end

ext_count uut(
.clk(clk),
.start(start),
.stop(stop),
.latch(latch),
.TxD(TxD),
.TxD_debug(TxD_debug)
);


endmodule
