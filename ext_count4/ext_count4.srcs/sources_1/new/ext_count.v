

module ext_count(input clk,input start,input stop,output reg[15:0] latch,output TxD_debug,output TxD);

wire TxD1;
wire TxD2;

reg TxDreg;
 wire txdone;
 wire txdone1;
wire var;
reg reset;
wire reset2;
wire txwr;
integer done;
 reg [15:0] outp;
parameter [1:0] IDLE = 2'b00, RUN = 2'b01, FIN = 2'b10;
 reg [1:0] state;
 integer count;
reg flag;
reg latchset;
wire T0;
reg flagop;


//wire T1;
//wire T2;
//wire T3;
wire T4;
//wire T5;
//wire T6;
//wire T7;
//reg start;

//reg stop;



initial begin
     count = 0;
    flag=0;
    outp =0;
    latch =0;
    reset=1;
   // start=0;
    //stop=0;
    flagop=0;
    state=2'b00;
    
 end

DFF uut4(T0,start,clk);

DFF uut5(T4,stop,clk);

//assign TxD=TxD1; 
//assign TxD=TxD2;




always @(posedge clk) begin
count=count+1;
if(count==5)
reset=0;
flagop =0;
/*if(count==5)
start=1;
else if(count==10)
start=0;
else if(count==504)
stop=1;
else if(count==600)
stop=0;*/

case(state)
       
        IDLE:begin 
                
                 if(T0==1) begin
                    state=2'b01;
                    outp=outp+1;
                    latch=0;
                    flag=0;
                 end
                 else if (T4==1) begin
                    state=2'b00;
                 end
                 else begin
                    state=2'b00;
                 end
                 end
        RUN:begin
                            
                if (T4 == 1) begin
                   state=2'b10;
                 end   
               else begin
                  outp=outp+1;
                  end         
                end
                   
         FIN: begin
            if(flag==0)begin
            latch=outp;
            flagop=1;
            
            end
            outp=0;
            flag=1;
             state=2'b00;
             end
             default:begin
                     state=2'b00;
                     end


       
       
endcase   
end
core uut(start, clk,TxD1,16'b0000001010001011,latch[7:0],flagop,txdone);


assign var=txdone;



core uut1(start, clk,TxD2,16'b0000001010001011,latch[15:8],var,txdone1);

assign TxD = TxD1 & TxD2;


//transmitter uut(clk,start,latch[7:0],TxD);
//Seven_seg uut(clk,stop,latch[15:0],seg7an[3:0],seg7seg[6:0]);
endmodule
	