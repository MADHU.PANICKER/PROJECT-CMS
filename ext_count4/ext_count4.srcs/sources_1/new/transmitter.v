`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company:
// Engineer:
//
// Create Date: 07/04/2015 11:29:02 AM
// Design Name:
// Module Name: transmitter
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:                    
//
//////////////////////////////////////////////////////////////////////////////////


module transmitter(
input clk, 
input reset, 
input [15:0] data,
 
output reg TxD 
    );

//internal variables
reg [3:0] bitcounter; 
reg [13:0] counter; //14 bits counter to count the baud rate, counter = clock / baud rate
reg state,nextstate; // initial & next state variable
// 10 bits data needed to be shifted out during transmission.
//The least significant bit is initialized with the binary value "0" (a start bit) A binary value "1" is introduced in the most significant bit
reg [9:0] rightshiftreg;


//reg [7:0] datal;
reg shift; //shift signal to start bit shifting in UART
reg load; //load signal to start loading the data into rightshift register and add start and stop bit
reg clear;
reg flag;
reg set; //clear signal to start reset the bitcounter for UART transmission

initial begin
state <= 0;
counter <= 0;
bitcounter <= 0;
//data <= 0;
//set <= 0;

//datah <= dataset[15:8];
flag=0;
end
//UART transmission logic
always @ (posedge clk)
begin
    if (reset) begin // reset is asserted (reset = 1)
        state <=0; // state is idle (state = 0)
        counter <=0; // counter for baud rate is reset to 0
        bitcounter <=0; //counter for bit transmission is reset to 0
    end
        else begin
         counter <= counter + 1; //counter for baud rate generator start counting
            if (counter >= 10415) //if count to 10416 (from 0 to 10415)
               begin              
                  state <= nextstate; //previous state change to next state
                  //counter <=0; // reset couter to 0
                  if (load) begin
                  rightshiftreg <= {1'b1,data,1'b0};
                  end//load the data if load is asserted
                  if (clear) begin
                   bitcounter <=0;
                   counter <= 0;
                   //set <= 0;
                   end // reset the bitcounter if clear is asserted
                  if (shift)
                     begin // if shift is asserted
                        rightshiftreg <= rightshiftreg >> 1; //right shift the data as we transmit the data from lsb
                        bitcounter <= bitcounter + 1; //count the bitcounter
                     end
               end
         end
end

//state machine

always @ (posedge clk) //trigger by positive edge of clock,
//always @ (state or bitcounter or transmit)
begin
    //if(transmit) begin
    load <=0; // set load equal to 0 at the beginning
    shift <=0; // set shift equal to 0 at the beginning
    clear <=0; // set clear equal to 0 at the beginning
    TxD <=1; // set TxD equals to during no transmission
    case (state)
        0: begin 
              // assert transmit input
             nextstate <= 1; // Move to transmit state
             load <=1; // set load to 1 to prepare to load the data
             shift <=0; // set shift to 0 so no shift ready yet
             clear <=0;
             //flag <= 0; // set clear to 0 to avoid clear any counter
            
           end
        1: begin  // transmit state
             if (bitcounter >=10) begin // check if transmission is complete or not. If complete
             nextstate <= 0; // set nextstate back to 0 to idle state
             clear <=1;
             // set clear to 1 to clear all counters
             end
             else begin // if transmisssion is not complete
             nextstate <= 1; // set nextstate to 1 to stay in transmit state
             TxD <= rightshiftreg[0]; // shift the bit to output TxD
             shift <=1; // set shift to 1 to continue shifting the data
             end
           end
         default: nextstate <= 0;                     
    endcase
    end



endmodule