// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Tue Feb 26 18:05:07 2019
// Host        : tanisha-Inspiron running 64-bit Ubuntu 18.04.1 LTS
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               /home/tanisha/Downloads/ext_count4/ext_count4.sim/sim_1/synth/timing/xsim/fsmsim_time_synth.v
// Design      : ext_count
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-3
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module DFF
   (T0,
    D,
    \latch_reg[0] ,
    reset_reg,
    \latch_reg[15] ,
    start_IBUF,
    clk_IBUF_BUFG,
    Q,
    T4,
    reset);
  output T0;
  output [0:0]D;
  output \latch_reg[0] ;
  output reset_reg;
  output \latch_reg[15] ;
  input start_IBUF;
  input clk_IBUF_BUFG;
  input [1:0]Q;
  input T4;
  input reset;

  wire [0:0]D;
  wire [1:0]Q;
  wire T0;
  wire T4;
  wire clk_IBUF_BUFG;
  wire \latch_reg[0] ;
  wire \latch_reg[15] ;
  wire reset;
  wire reset_reg;
  wire start_IBUF;

  FDRE #(
    .INIT(1'b0)) 
    Q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(start_IBUF),
        .Q(T0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \latch[15]_i_1 
       (.I0(Q[0]),
        .I1(T0),
        .I2(Q[1]),
        .O(\latch_reg[15] ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h002E)) 
    \latch[15]_i_2 
       (.I0(T0),
        .I1(Q[1]),
        .I2(reset),
        .I3(Q[0]),
        .O(\latch_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFB0A)) 
    reset_i_1
       (.I0(Q[1]),
        .I1(T0),
        .I2(Q[0]),
        .I3(reset),
        .O(reset_reg));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'h002E)) 
    \state[0]_i_1 
       (.I0(T0),
        .I1(Q[0]),
        .I2(T4),
        .I3(Q[1]),
        .O(D));
endmodule

(* ORIG_REF_NAME = "DFF" *) 
module DFF_0
   (T4,
    D,
    \outp_reg[0] ,
    stop,
    clk_IBUF_BUFG,
    Q,
    T0);
  output T4;
  output [0:0]D;
  output \outp_reg[0] ;
  input stop;
  input clk_IBUF_BUFG;
  input [1:0]Q;
  input T0;

  wire [0:0]D;
  wire [1:0]Q;
  wire T0;
  wire T4;
  wire clk_IBUF_BUFG;
  wire \outp_reg[0] ;
  wire stop;

  FDRE #(
    .INIT(1'b0)) 
    Q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(stop),
        .Q(T4),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h3374)) 
    \outp[15]_i_2 
       (.I0(T4),
        .I1(Q[0]),
        .I2(T0),
        .I3(Q[1]),
        .O(\outp_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \state[1]_i_1 
       (.I0(Q[0]),
        .I1(T4),
        .I2(Q[1]),
        .O(D));
endmodule

module Seven_seg
   (seg7an_OBUF,
    seg7seg_OBUF,
    clk_IBUF_BUFG,
    stop,
    latch_OBUF);
  output [3:0]seg7an_OBUF;
  output [6:0]seg7seg_OBUF;
  input clk_IBUF_BUFG;
  input stop;
  input [15:0]latch_OBUF;

  wire [3:0]LED_BCD__31;
  wire [1:0]LED_activating_counter;
  wire clk_IBUF_BUFG;
  wire [15:0]latch_OBUF;
  wire \refresh_counter[0]_i_2_n_0 ;
  wire \refresh_counter_reg[0]_i_1_n_0 ;
  wire \refresh_counter_reg[0]_i_1_n_1 ;
  wire \refresh_counter_reg[0]_i_1_n_2 ;
  wire \refresh_counter_reg[0]_i_1_n_3 ;
  wire \refresh_counter_reg[0]_i_1_n_4 ;
  wire \refresh_counter_reg[0]_i_1_n_5 ;
  wire \refresh_counter_reg[0]_i_1_n_6 ;
  wire \refresh_counter_reg[0]_i_1_n_7 ;
  wire \refresh_counter_reg[12]_i_1_n_0 ;
  wire \refresh_counter_reg[12]_i_1_n_1 ;
  wire \refresh_counter_reg[12]_i_1_n_2 ;
  wire \refresh_counter_reg[12]_i_1_n_3 ;
  wire \refresh_counter_reg[12]_i_1_n_4 ;
  wire \refresh_counter_reg[12]_i_1_n_5 ;
  wire \refresh_counter_reg[12]_i_1_n_6 ;
  wire \refresh_counter_reg[12]_i_1_n_7 ;
  wire \refresh_counter_reg[16]_i_1_n_1 ;
  wire \refresh_counter_reg[16]_i_1_n_2 ;
  wire \refresh_counter_reg[16]_i_1_n_3 ;
  wire \refresh_counter_reg[16]_i_1_n_4 ;
  wire \refresh_counter_reg[16]_i_1_n_5 ;
  wire \refresh_counter_reg[16]_i_1_n_6 ;
  wire \refresh_counter_reg[16]_i_1_n_7 ;
  wire \refresh_counter_reg[4]_i_1_n_0 ;
  wire \refresh_counter_reg[4]_i_1_n_1 ;
  wire \refresh_counter_reg[4]_i_1_n_2 ;
  wire \refresh_counter_reg[4]_i_1_n_3 ;
  wire \refresh_counter_reg[4]_i_1_n_4 ;
  wire \refresh_counter_reg[4]_i_1_n_5 ;
  wire \refresh_counter_reg[4]_i_1_n_6 ;
  wire \refresh_counter_reg[4]_i_1_n_7 ;
  wire \refresh_counter_reg[8]_i_1_n_0 ;
  wire \refresh_counter_reg[8]_i_1_n_1 ;
  wire \refresh_counter_reg[8]_i_1_n_2 ;
  wire \refresh_counter_reg[8]_i_1_n_3 ;
  wire \refresh_counter_reg[8]_i_1_n_4 ;
  wire \refresh_counter_reg[8]_i_1_n_5 ;
  wire \refresh_counter_reg[8]_i_1_n_6 ;
  wire \refresh_counter_reg[8]_i_1_n_7 ;
  wire \refresh_counter_reg_n_0_[0] ;
  wire \refresh_counter_reg_n_0_[10] ;
  wire \refresh_counter_reg_n_0_[11] ;
  wire \refresh_counter_reg_n_0_[12] ;
  wire \refresh_counter_reg_n_0_[13] ;
  wire \refresh_counter_reg_n_0_[14] ;
  wire \refresh_counter_reg_n_0_[15] ;
  wire \refresh_counter_reg_n_0_[16] ;
  wire \refresh_counter_reg_n_0_[17] ;
  wire \refresh_counter_reg_n_0_[1] ;
  wire \refresh_counter_reg_n_0_[2] ;
  wire \refresh_counter_reg_n_0_[3] ;
  wire \refresh_counter_reg_n_0_[4] ;
  wire \refresh_counter_reg_n_0_[5] ;
  wire \refresh_counter_reg_n_0_[6] ;
  wire \refresh_counter_reg_n_0_[7] ;
  wire \refresh_counter_reg_n_0_[8] ;
  wire \refresh_counter_reg_n_0_[9] ;
  wire [3:0]seg7an_OBUF;
  wire [6:0]seg7seg_OBUF;
  wire stop;
  wire [3:3]\NLW_refresh_counter_reg[16]_i_1_CO_UNCONNECTED ;

  LUT1 #(
    .INIT(2'h1)) 
    \refresh_counter[0]_i_2 
       (.I0(\refresh_counter_reg_n_0_[0] ),
        .O(\refresh_counter[0]_i_2_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[0]_i_1_n_7 ),
        .Q(\refresh_counter_reg_n_0_[0] ));
  CARRY4 \refresh_counter_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\refresh_counter_reg[0]_i_1_n_0 ,\refresh_counter_reg[0]_i_1_n_1 ,\refresh_counter_reg[0]_i_1_n_2 ,\refresh_counter_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\refresh_counter_reg[0]_i_1_n_4 ,\refresh_counter_reg[0]_i_1_n_5 ,\refresh_counter_reg[0]_i_1_n_6 ,\refresh_counter_reg[0]_i_1_n_7 }),
        .S({\refresh_counter_reg_n_0_[3] ,\refresh_counter_reg_n_0_[2] ,\refresh_counter_reg_n_0_[1] ,\refresh_counter[0]_i_2_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[8]_i_1_n_5 ),
        .Q(\refresh_counter_reg_n_0_[10] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[8]_i_1_n_4 ),
        .Q(\refresh_counter_reg_n_0_[11] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[12]_i_1_n_7 ),
        .Q(\refresh_counter_reg_n_0_[12] ));
  CARRY4 \refresh_counter_reg[12]_i_1 
       (.CI(\refresh_counter_reg[8]_i_1_n_0 ),
        .CO({\refresh_counter_reg[12]_i_1_n_0 ,\refresh_counter_reg[12]_i_1_n_1 ,\refresh_counter_reg[12]_i_1_n_2 ,\refresh_counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refresh_counter_reg[12]_i_1_n_4 ,\refresh_counter_reg[12]_i_1_n_5 ,\refresh_counter_reg[12]_i_1_n_6 ,\refresh_counter_reg[12]_i_1_n_7 }),
        .S({\refresh_counter_reg_n_0_[15] ,\refresh_counter_reg_n_0_[14] ,\refresh_counter_reg_n_0_[13] ,\refresh_counter_reg_n_0_[12] }));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[12]_i_1_n_6 ),
        .Q(\refresh_counter_reg_n_0_[13] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[12]_i_1_n_5 ),
        .Q(\refresh_counter_reg_n_0_[14] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[12]_i_1_n_4 ),
        .Q(\refresh_counter_reg_n_0_[15] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[16]_i_1_n_7 ),
        .Q(\refresh_counter_reg_n_0_[16] ));
  CARRY4 \refresh_counter_reg[16]_i_1 
       (.CI(\refresh_counter_reg[12]_i_1_n_0 ),
        .CO({\NLW_refresh_counter_reg[16]_i_1_CO_UNCONNECTED [3],\refresh_counter_reg[16]_i_1_n_1 ,\refresh_counter_reg[16]_i_1_n_2 ,\refresh_counter_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refresh_counter_reg[16]_i_1_n_4 ,\refresh_counter_reg[16]_i_1_n_5 ,\refresh_counter_reg[16]_i_1_n_6 ,\refresh_counter_reg[16]_i_1_n_7 }),
        .S({LED_activating_counter,\refresh_counter_reg_n_0_[17] ,\refresh_counter_reg_n_0_[16] }));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[16]_i_1_n_6 ),
        .Q(\refresh_counter_reg_n_0_[17] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[16]_i_1_n_5 ),
        .Q(LED_activating_counter[0]));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[16]_i_1_n_4 ),
        .Q(LED_activating_counter[1]));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[0]_i_1_n_6 ),
        .Q(\refresh_counter_reg_n_0_[1] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[0]_i_1_n_5 ),
        .Q(\refresh_counter_reg_n_0_[2] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[0]_i_1_n_4 ),
        .Q(\refresh_counter_reg_n_0_[3] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[4]_i_1_n_7 ),
        .Q(\refresh_counter_reg_n_0_[4] ));
  CARRY4 \refresh_counter_reg[4]_i_1 
       (.CI(\refresh_counter_reg[0]_i_1_n_0 ),
        .CO({\refresh_counter_reg[4]_i_1_n_0 ,\refresh_counter_reg[4]_i_1_n_1 ,\refresh_counter_reg[4]_i_1_n_2 ,\refresh_counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refresh_counter_reg[4]_i_1_n_4 ,\refresh_counter_reg[4]_i_1_n_5 ,\refresh_counter_reg[4]_i_1_n_6 ,\refresh_counter_reg[4]_i_1_n_7 }),
        .S({\refresh_counter_reg_n_0_[7] ,\refresh_counter_reg_n_0_[6] ,\refresh_counter_reg_n_0_[5] ,\refresh_counter_reg_n_0_[4] }));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[4]_i_1_n_6 ),
        .Q(\refresh_counter_reg_n_0_[5] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[4]_i_1_n_5 ),
        .Q(\refresh_counter_reg_n_0_[6] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[4]_i_1_n_4 ),
        .Q(\refresh_counter_reg_n_0_[7] ));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[8]_i_1_n_7 ),
        .Q(\refresh_counter_reg_n_0_[8] ));
  CARRY4 \refresh_counter_reg[8]_i_1 
       (.CI(\refresh_counter_reg[4]_i_1_n_0 ),
        .CO({\refresh_counter_reg[8]_i_1_n_0 ,\refresh_counter_reg[8]_i_1_n_1 ,\refresh_counter_reg[8]_i_1_n_2 ,\refresh_counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\refresh_counter_reg[8]_i_1_n_4 ,\refresh_counter_reg[8]_i_1_n_5 ,\refresh_counter_reg[8]_i_1_n_6 ,\refresh_counter_reg[8]_i_1_n_7 }),
        .S({\refresh_counter_reg_n_0_[11] ,\refresh_counter_reg_n_0_[10] ,\refresh_counter_reg_n_0_[9] ,\refresh_counter_reg_n_0_[8] }));
  FDCE #(
    .INIT(1'b0)) 
    \refresh_counter_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .CLR(stop),
        .D(\refresh_counter_reg[8]_i_1_n_6 ),
        .Q(\refresh_counter_reg_n_0_[9] ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \seg7an_OBUF[0]_inst_i_1 
       (.I0(LED_activating_counter[1]),
        .I1(LED_activating_counter[0]),
        .O(seg7an_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \seg7an_OBUF[1]_inst_i_1 
       (.I0(LED_activating_counter[0]),
        .I1(LED_activating_counter[1]),
        .O(seg7an_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \seg7an_OBUF[2]_inst_i_1 
       (.I0(LED_activating_counter[1]),
        .I1(LED_activating_counter[0]),
        .O(seg7an_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \seg7an_OBUF[3]_inst_i_1 
       (.I0(LED_activating_counter[1]),
        .I1(LED_activating_counter[0]),
        .O(seg7an_OBUF[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h4025)) 
    \seg7seg_OBUF[0]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[0]),
        .I2(LED_BCD__31[2]),
        .I3(LED_BCD__31[1]),
        .O(seg7seg_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h5190)) 
    \seg7seg_OBUF[1]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[2]),
        .I2(LED_BCD__31[0]),
        .I3(LED_BCD__31[1]),
        .O(seg7seg_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h5710)) 
    \seg7seg_OBUF[2]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[1]),
        .I2(LED_BCD__31[2]),
        .I3(LED_BCD__31[0]),
        .O(seg7seg_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hC214)) 
    \seg7seg_OBUF[3]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[2]),
        .I2(LED_BCD__31[0]),
        .I3(LED_BCD__31[1]),
        .O(seg7seg_OBUF[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hA210)) 
    \seg7seg_OBUF[4]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[0]),
        .I2(LED_BCD__31[1]),
        .I3(LED_BCD__31[2]),
        .O(seg7seg_OBUF[4]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hAC48)) 
    \seg7seg_OBUF[5]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[2]),
        .I2(LED_BCD__31[0]),
        .I3(LED_BCD__31[1]),
        .O(seg7seg_OBUF[5]));
  LUT4 #(
    .INIT(16'h2094)) 
    \seg7seg_OBUF[6]_inst_i_1 
       (.I0(LED_BCD__31[3]),
        .I1(LED_BCD__31[2]),
        .I2(LED_BCD__31[0]),
        .I3(LED_BCD__31[1]),
        .O(seg7seg_OBUF[6]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \seg7seg_OBUF[6]_inst_i_2 
       (.I0(latch_OBUF[11]),
        .I1(latch_OBUF[15]),
        .I2(latch_OBUF[3]),
        .I3(LED_activating_counter[1]),
        .I4(LED_activating_counter[0]),
        .I5(latch_OBUF[7]),
        .O(LED_BCD__31[3]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \seg7seg_OBUF[6]_inst_i_3 
       (.I0(latch_OBUF[10]),
        .I1(latch_OBUF[14]),
        .I2(latch_OBUF[2]),
        .I3(LED_activating_counter[1]),
        .I4(LED_activating_counter[0]),
        .I5(latch_OBUF[6]),
        .O(LED_BCD__31[2]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \seg7seg_OBUF[6]_inst_i_4 
       (.I0(latch_OBUF[8]),
        .I1(latch_OBUF[12]),
        .I2(latch_OBUF[0]),
        .I3(LED_activating_counter[1]),
        .I4(LED_activating_counter[0]),
        .I5(latch_OBUF[4]),
        .O(LED_BCD__31[0]));
  LUT6 #(
    .INIT(64'hF0AAFFCCF0AA00CC)) 
    \seg7seg_OBUF[6]_inst_i_5 
       (.I0(latch_OBUF[9]),
        .I1(latch_OBUF[13]),
        .I2(latch_OBUF[1]),
        .I3(LED_activating_counter[1]),
        .I4(LED_activating_counter[0]),
        .I5(latch_OBUF[5]),
        .O(LED_BCD__31[1]));
endmodule

(* FIN = "2'b10" *) (* IDLE = "2'b00" *) (* RUN = "2'b01" *) 
(* NotValidForBitStream *)
module ext_count
   (clk,
    start,
    stop,
    latch,
    seg7seg,
    seg7an);
  input clk;
  input start;
  input stop;
  output [15:0]latch;
  output [6:0]seg7seg;
  output [3:0]seg7an;

  wire T0;
  wire T4;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire [15:0]latch;
  wire [15:0]latch_OBUF;
  wire [15:0]outp;
  wire [15:1]outp0;
  wire \outp[0]_i_1_n_0 ;
  wire \outp[15]_i_1_n_0 ;
  wire \outp_reg[12]_i_1_n_0 ;
  wire \outp_reg[12]_i_1_n_1 ;
  wire \outp_reg[12]_i_1_n_2 ;
  wire \outp_reg[12]_i_1_n_3 ;
  wire \outp_reg[15]_i_3_n_2 ;
  wire \outp_reg[15]_i_3_n_3 ;
  wire \outp_reg[4]_i_1_n_0 ;
  wire \outp_reg[4]_i_1_n_1 ;
  wire \outp_reg[4]_i_1_n_2 ;
  wire \outp_reg[4]_i_1_n_3 ;
  wire \outp_reg[8]_i_1_n_0 ;
  wire \outp_reg[8]_i_1_n_1 ;
  wire \outp_reg[8]_i_1_n_2 ;
  wire \outp_reg[8]_i_1_n_3 ;
  wire reset;
  wire [3:0]seg7an;
  wire [3:0]seg7an_OBUF;
  wire [6:0]seg7seg;
  wire [6:0]seg7seg_OBUF;
  wire start;
  wire start_IBUF;
  wire [1:0]state;
  wire \state_reg_n_0_[0] ;
  wire \state_reg_n_0_[1] ;
  wire stop;
  wire stop_IBUF;
  wire uut4_n_2;
  wire uut4_n_3;
  wire uut4_n_4;
  wire uut5_n_2;
  wire [3:2]\NLW_outp_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_outp_reg[15]_i_3_O_UNCONNECTED ;

initial begin
 $sdf_annotate("fsmsim_time_synth.sdf",,,,"tool_control");
end
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  OBUF \latch_OBUF[0]_inst 
       (.I(latch_OBUF[0]),
        .O(latch[0]));
  OBUF \latch_OBUF[10]_inst 
       (.I(latch_OBUF[10]),
        .O(latch[10]));
  OBUF \latch_OBUF[11]_inst 
       (.I(latch_OBUF[11]),
        .O(latch[11]));
  OBUF \latch_OBUF[12]_inst 
       (.I(latch_OBUF[12]),
        .O(latch[12]));
  OBUF \latch_OBUF[13]_inst 
       (.I(latch_OBUF[13]),
        .O(latch[13]));
  OBUF \latch_OBUF[14]_inst 
       (.I(latch_OBUF[14]),
        .O(latch[14]));
  OBUF \latch_OBUF[15]_inst 
       (.I(latch_OBUF[15]),
        .O(latch[15]));
  OBUF \latch_OBUF[1]_inst 
       (.I(latch_OBUF[1]),
        .O(latch[1]));
  OBUF \latch_OBUF[2]_inst 
       (.I(latch_OBUF[2]),
        .O(latch[2]));
  OBUF \latch_OBUF[3]_inst 
       (.I(latch_OBUF[3]),
        .O(latch[3]));
  OBUF \latch_OBUF[4]_inst 
       (.I(latch_OBUF[4]),
        .O(latch[4]));
  OBUF \latch_OBUF[5]_inst 
       (.I(latch_OBUF[5]),
        .O(latch[5]));
  OBUF \latch_OBUF[6]_inst 
       (.I(latch_OBUF[6]),
        .O(latch[6]));
  OBUF \latch_OBUF[7]_inst 
       (.I(latch_OBUF[7]),
        .O(latch[7]));
  OBUF \latch_OBUF[8]_inst 
       (.I(latch_OBUF[8]),
        .O(latch[8]));
  OBUF \latch_OBUF[9]_inst 
       (.I(latch_OBUF[9]),
        .O(latch[9]));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[0]),
        .Q(latch_OBUF[0]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[10]),
        .Q(latch_OBUF[10]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[11]),
        .Q(latch_OBUF[11]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[12]),
        .Q(latch_OBUF[12]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[13]),
        .Q(latch_OBUF[13]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[14]),
        .Q(latch_OBUF[14]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[15]),
        .Q(latch_OBUF[15]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[1]),
        .Q(latch_OBUF[1]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[2]),
        .Q(latch_OBUF[2]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[3]),
        .Q(latch_OBUF[3]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[4]),
        .Q(latch_OBUF[4]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[5]),
        .Q(latch_OBUF[5]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[6]),
        .Q(latch_OBUF[6]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[7]),
        .Q(latch_OBUF[7]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[8]),
        .Q(latch_OBUF[8]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[9]),
        .Q(latch_OBUF[9]),
        .R(uut4_n_4));
  LUT1 #(
    .INIT(2'h1)) 
    \outp[0]_i_1 
       (.I0(outp[0]),
        .O(\outp[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \outp[15]_i_1 
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state_reg_n_0_[0] ),
        .O(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(\outp[0]_i_1_n_0 ),
        .Q(outp[0]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[10]),
        .Q(outp[10]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[11]),
        .Q(outp[11]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[12]),
        .Q(outp[12]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[12]_i_1 
       (.CI(\outp_reg[8]_i_1_n_0 ),
        .CO({\outp_reg[12]_i_1_n_0 ,\outp_reg[12]_i_1_n_1 ,\outp_reg[12]_i_1_n_2 ,\outp_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(outp0[12:9]),
        .S(outp[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[13]),
        .Q(outp[13]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[14]),
        .Q(outp[14]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[15]),
        .Q(outp[15]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[15]_i_3 
       (.CI(\outp_reg[12]_i_1_n_0 ),
        .CO({\NLW_outp_reg[15]_i_3_CO_UNCONNECTED [3:2],\outp_reg[15]_i_3_n_2 ,\outp_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_outp_reg[15]_i_3_O_UNCONNECTED [3],outp0[15:13]}),
        .S({1'b0,outp[15:13]}));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[1]),
        .Q(outp[1]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[2]),
        .Q(outp[2]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[3]),
        .Q(outp[3]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[4]),
        .Q(outp[4]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\outp_reg[4]_i_1_n_0 ,\outp_reg[4]_i_1_n_1 ,\outp_reg[4]_i_1_n_2 ,\outp_reg[4]_i_1_n_3 }),
        .CYINIT(outp[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(outp0[4:1]),
        .S(outp[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[5]),
        .Q(outp[5]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[6]),
        .Q(outp[6]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[7]),
        .Q(outp[7]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[8]),
        .Q(outp[8]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[8]_i_1 
       (.CI(\outp_reg[4]_i_1_n_0 ),
        .CO({\outp_reg[8]_i_1_n_0 ,\outp_reg[8]_i_1_n_1 ,\outp_reg[8]_i_1_n_2 ,\outp_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(outp0[8:5]),
        .S(outp[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[9]),
        .Q(outp[9]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    reset_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(uut4_n_3),
        .Q(reset),
        .R(1'b0));
  OBUF \seg7an_OBUF[0]_inst 
       (.I(seg7an_OBUF[0]),
        .O(seg7an[0]));
  OBUF \seg7an_OBUF[1]_inst 
       (.I(seg7an_OBUF[1]),
        .O(seg7an[1]));
  OBUF \seg7an_OBUF[2]_inst 
       (.I(seg7an_OBUF[2]),
        .O(seg7an[2]));
  OBUF \seg7an_OBUF[3]_inst 
       (.I(seg7an_OBUF[3]),
        .O(seg7an[3]));
  OBUF \seg7seg_OBUF[0]_inst 
       (.I(seg7seg_OBUF[0]),
        .O(seg7seg[0]));
  OBUF \seg7seg_OBUF[1]_inst 
       (.I(seg7seg_OBUF[1]),
        .O(seg7seg[1]));
  OBUF \seg7seg_OBUF[2]_inst 
       (.I(seg7seg_OBUF[2]),
        .O(seg7seg[2]));
  OBUF \seg7seg_OBUF[3]_inst 
       (.I(seg7seg_OBUF[3]),
        .O(seg7seg[3]));
  OBUF \seg7seg_OBUF[4]_inst 
       (.I(seg7seg_OBUF[4]),
        .O(seg7seg[4]));
  OBUF \seg7seg_OBUF[5]_inst 
       (.I(seg7seg_OBUF[5]),
        .O(seg7seg[5]));
  OBUF \seg7seg_OBUF[6]_inst 
       (.I(seg7seg_OBUF[6]),
        .O(seg7seg[6]));
  IBUF start_IBUF_inst
       (.I(start),
        .O(start_IBUF));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(state[0]),
        .Q(\state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(state[1]),
        .Q(\state_reg_n_0_[1] ),
        .R(1'b0));
  IBUF stop_IBUF_inst
       (.I(stop),
        .O(stop_IBUF));
  Seven_seg uut
       (.clk_IBUF_BUFG(clk_IBUF_BUFG),
        .latch_OBUF(latch_OBUF),
        .seg7an_OBUF(seg7an_OBUF),
        .seg7seg_OBUF(seg7seg_OBUF),
        .stop(stop_IBUF));
  DFF uut4
       (.D(state[0]),
        .Q({\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .T0(T0),
        .T4(T4),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\latch_reg[0] (uut4_n_2),
        .\latch_reg[15] (uut4_n_4),
        .reset(reset),
        .reset_reg(uut4_n_3),
        .start_IBUF(start_IBUF));
  DFF_0 uut5
       (.D(state[1]),
        .Q({\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .T0(T0),
        .T4(T4),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\outp_reg[0] (uut5_n_2),
        .stop(stop_IBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
