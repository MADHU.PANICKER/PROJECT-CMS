// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (lin64) Build 2258646 Thu Jun 14 20:02:38 MDT 2018
// Date        : Thu Mar  7 22:26:09 2019
// Host        : tanisha-Inspiron running 64-bit Ubuntu 18.04.1 LTS
// Command     : write_verilog -mode funcsim -nolib -force -file
//               /home/tanisha/Downloads/ext_count4/ext_count4.sim/sim_1/synth/func/xsim/fsmsim_func_synth.v
// Design      : ext_count
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-3
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module DFF
   (T0,
    D,
    \latch_reg[0] ,
    reset_reg,
    \latch_reg[15] ,
    start,
    clk_IBUF_BUFG,
    Q,
    T4,
    reset);
  output T0;
  output [0:0]D;
  output \latch_reg[0] ;
  output reset_reg;
  output \latch_reg[15] ;
  input start;
  input clk_IBUF_BUFG;
  input [1:0]Q;
  input T4;
  input reset;

  wire [0:0]D;
  wire [1:0]Q;
  wire T0;
  wire T4;
  wire clk_IBUF_BUFG;
  wire \latch_reg[0] ;
  wire \latch_reg[15] ;
  wire reset;
  wire reset_reg;
  wire start;

  FDRE #(
    .INIT(1'b0)) 
    Q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(start),
        .Q(T0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \latch[15]_i_1 
       (.I0(Q[0]),
        .I1(T0),
        .I2(Q[1]),
        .O(\latch_reg[15] ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h002E)) 
    \latch[15]_i_2 
       (.I0(T0),
        .I1(Q[1]),
        .I2(reset),
        .I3(Q[0]),
        .O(\latch_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFB0A)) 
    reset_i_1
       (.I0(Q[1]),
        .I1(T0),
        .I2(Q[0]),
        .I3(reset),
        .O(reset_reg));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h002E)) 
    \state[0]_i_1 
       (.I0(T0),
        .I1(Q[0]),
        .I2(T4),
        .I3(Q[1]),
        .O(D));
endmodule

(* ORIG_REF_NAME = "DFF" *) 
module DFF_0
   (T4,
    D,
    \outp_reg[0] ,
    stop_reg,
    clk_IBUF_BUFG,
    Q,
    T0);
  output T4;
  output [0:0]D;
  output \outp_reg[0] ;
  input stop_reg;
  input clk_IBUF_BUFG;
  input [1:0]Q;
  input T0;

  wire [0:0]D;
  wire [1:0]Q;
  wire T0;
  wire T4;
  wire clk_IBUF_BUFG;
  wire \outp_reg[0] ;
  wire stop_reg;

  FDRE #(
    .INIT(1'b0)) 
    Q_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(stop_reg),
        .Q(T4),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'h3374)) 
    \outp[15]_i_2 
       (.I0(T4),
        .I1(Q[0]),
        .I2(T0),
        .I3(Q[1]),
        .O(\outp_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \state[1]_i_1 
       (.I0(Q[0]),
        .I1(T4),
        .I2(Q[1]),
        .O(D));
endmodule

(* FIN = "2'b10" *) (* IDLE = "2'b00" *) (* RUN = "2'b01" *) 
(* NotValidForBitStream *)
module ext_count
   (clk,
    latch,
    TxD);
  input clk;
  output [15:0]latch;
  output TxD;

  wire T0;
  wire T4;
  wire TxD;
  wire TxD_OBUF;
  wire clk;
  wire clk_IBUF;
  wire clk_IBUF_BUFG;
  wire \count[0]_i_2_n_0 ;
  wire [31:0]count_reg;
  wire \count_reg[0]_i_1_n_0 ;
  wire \count_reg[0]_i_1_n_1 ;
  wire \count_reg[0]_i_1_n_2 ;
  wire \count_reg[0]_i_1_n_3 ;
  wire \count_reg[0]_i_1_n_4 ;
  wire \count_reg[0]_i_1_n_5 ;
  wire \count_reg[0]_i_1_n_6 ;
  wire \count_reg[0]_i_1_n_7 ;
  wire \count_reg[12]_i_1_n_0 ;
  wire \count_reg[12]_i_1_n_1 ;
  wire \count_reg[12]_i_1_n_2 ;
  wire \count_reg[12]_i_1_n_3 ;
  wire \count_reg[12]_i_1_n_4 ;
  wire \count_reg[12]_i_1_n_5 ;
  wire \count_reg[12]_i_1_n_6 ;
  wire \count_reg[12]_i_1_n_7 ;
  wire \count_reg[16]_i_1_n_0 ;
  wire \count_reg[16]_i_1_n_1 ;
  wire \count_reg[16]_i_1_n_2 ;
  wire \count_reg[16]_i_1_n_3 ;
  wire \count_reg[16]_i_1_n_4 ;
  wire \count_reg[16]_i_1_n_5 ;
  wire \count_reg[16]_i_1_n_6 ;
  wire \count_reg[16]_i_1_n_7 ;
  wire \count_reg[20]_i_1_n_0 ;
  wire \count_reg[20]_i_1_n_1 ;
  wire \count_reg[20]_i_1_n_2 ;
  wire \count_reg[20]_i_1_n_3 ;
  wire \count_reg[20]_i_1_n_4 ;
  wire \count_reg[20]_i_1_n_5 ;
  wire \count_reg[20]_i_1_n_6 ;
  wire \count_reg[20]_i_1_n_7 ;
  wire \count_reg[24]_i_1_n_0 ;
  wire \count_reg[24]_i_1_n_1 ;
  wire \count_reg[24]_i_1_n_2 ;
  wire \count_reg[24]_i_1_n_3 ;
  wire \count_reg[24]_i_1_n_4 ;
  wire \count_reg[24]_i_1_n_5 ;
  wire \count_reg[24]_i_1_n_6 ;
  wire \count_reg[24]_i_1_n_7 ;
  wire \count_reg[28]_i_1_n_1 ;
  wire \count_reg[28]_i_1_n_2 ;
  wire \count_reg[28]_i_1_n_3 ;
  wire \count_reg[28]_i_1_n_4 ;
  wire \count_reg[28]_i_1_n_5 ;
  wire \count_reg[28]_i_1_n_6 ;
  wire \count_reg[28]_i_1_n_7 ;
  wire \count_reg[4]_i_1_n_0 ;
  wire \count_reg[4]_i_1_n_1 ;
  wire \count_reg[4]_i_1_n_2 ;
  wire \count_reg[4]_i_1_n_3 ;
  wire \count_reg[4]_i_1_n_4 ;
  wire \count_reg[4]_i_1_n_5 ;
  wire \count_reg[4]_i_1_n_6 ;
  wire \count_reg[4]_i_1_n_7 ;
  wire \count_reg[8]_i_1_n_0 ;
  wire \count_reg[8]_i_1_n_1 ;
  wire \count_reg[8]_i_1_n_2 ;
  wire \count_reg[8]_i_1_n_3 ;
  wire \count_reg[8]_i_1_n_4 ;
  wire \count_reg[8]_i_1_n_5 ;
  wire \count_reg[8]_i_1_n_6 ;
  wire \count_reg[8]_i_1_n_7 ;
  wire [15:0]latch;
  wire [15:0]latch_OBUF;
  wire [15:0]outp;
  wire [15:1]outp0;
  wire \outp[0]_i_1_n_0 ;
  wire \outp[15]_i_1_n_0 ;
  wire \outp_reg[12]_i_1_n_0 ;
  wire \outp_reg[12]_i_1_n_1 ;
  wire \outp_reg[12]_i_1_n_2 ;
  wire \outp_reg[12]_i_1_n_3 ;
  wire \outp_reg[15]_i_3_n_2 ;
  wire \outp_reg[15]_i_3_n_3 ;
  wire \outp_reg[4]_i_1_n_0 ;
  wire \outp_reg[4]_i_1_n_1 ;
  wire \outp_reg[4]_i_1_n_2 ;
  wire \outp_reg[4]_i_1_n_3 ;
  wire \outp_reg[8]_i_1_n_0 ;
  wire \outp_reg[8]_i_1_n_1 ;
  wire \outp_reg[8]_i_1_n_2 ;
  wire \outp_reg[8]_i_1_n_3 ;
  wire reset;
  wire start;
  wire start_i_10_n_0;
  wire start_i_11_n_0;
  wire start_i_13_n_0;
  wire start_i_14_n_0;
  wire start_i_15_n_0;
  wire start_i_16_n_0;
  wire start_i_1_n_0;
  wire start_i_21_n_0;
  wire start_i_22_n_0;
  wire start_i_23_n_0;
  wire start_i_24_n_0;
  wire start_i_25_n_0;
  wire start_i_26_n_0;
  wire start_i_27_n_0;
  wire start_i_28_n_0;
  wire start_i_32_n_0;
  wire start_i_33_n_0;
  wire start_i_34_n_0;
  wire start_i_35_n_0;
  wire start_i_5_n_0;
  wire start_i_6_n_0;
  wire start_i_7_n_0;
  wire start_i_9_n_0;
  wire start_reg_i_12_n_0;
  wire start_reg_i_12_n_1;
  wire start_reg_i_12_n_2;
  wire start_reg_i_12_n_3;
  wire start_reg_i_17_n_2;
  wire start_reg_i_17_n_3;
  wire start_reg_i_17_n_5;
  wire start_reg_i_17_n_6;
  wire start_reg_i_17_n_7;
  wire start_reg_i_18_n_0;
  wire start_reg_i_18_n_1;
  wire start_reg_i_18_n_2;
  wire start_reg_i_18_n_3;
  wire start_reg_i_18_n_4;
  wire start_reg_i_18_n_5;
  wire start_reg_i_18_n_6;
  wire start_reg_i_18_n_7;
  wire start_reg_i_19_n_0;
  wire start_reg_i_19_n_1;
  wire start_reg_i_19_n_2;
  wire start_reg_i_19_n_3;
  wire start_reg_i_19_n_4;
  wire start_reg_i_19_n_5;
  wire start_reg_i_19_n_6;
  wire start_reg_i_19_n_7;
  wire start_reg_i_20_n_0;
  wire start_reg_i_20_n_1;
  wire start_reg_i_20_n_2;
  wire start_reg_i_20_n_3;
  wire start_reg_i_29_n_0;
  wire start_reg_i_29_n_1;
  wire start_reg_i_29_n_2;
  wire start_reg_i_29_n_3;
  wire start_reg_i_29_n_4;
  wire start_reg_i_29_n_5;
  wire start_reg_i_29_n_6;
  wire start_reg_i_29_n_7;
  wire start_reg_i_2_n_1;
  wire start_reg_i_2_n_2;
  wire start_reg_i_2_n_3;
  wire start_reg_i_30_n_0;
  wire start_reg_i_30_n_1;
  wire start_reg_i_30_n_2;
  wire start_reg_i_30_n_3;
  wire start_reg_i_30_n_4;
  wire start_reg_i_30_n_5;
  wire start_reg_i_30_n_6;
  wire start_reg_i_30_n_7;
  wire start_reg_i_31_n_0;
  wire start_reg_i_31_n_1;
  wire start_reg_i_31_n_2;
  wire start_reg_i_31_n_3;
  wire start_reg_i_31_n_4;
  wire start_reg_i_31_n_5;
  wire start_reg_i_31_n_6;
  wire start_reg_i_31_n_7;
  wire start_reg_i_36_n_0;
  wire start_reg_i_36_n_1;
  wire start_reg_i_36_n_2;
  wire start_reg_i_36_n_3;
  wire start_reg_i_36_n_4;
  wire start_reg_i_36_n_5;
  wire start_reg_i_36_n_6;
  wire start_reg_i_36_n_7;
  wire start_reg_i_37_n_0;
  wire start_reg_i_37_n_1;
  wire start_reg_i_37_n_2;
  wire start_reg_i_37_n_3;
  wire start_reg_i_37_n_4;
  wire start_reg_i_37_n_5;
  wire start_reg_i_37_n_6;
  wire start_reg_i_37_n_7;
  wire start_reg_i_3_n_1;
  wire start_reg_i_3_n_2;
  wire start_reg_i_3_n_3;
  wire start_reg_i_4_n_0;
  wire start_reg_i_4_n_1;
  wire start_reg_i_4_n_2;
  wire start_reg_i_4_n_3;
  wire start_reg_i_8_n_0;
  wire start_reg_i_8_n_1;
  wire start_reg_i_8_n_2;
  wire start_reg_i_8_n_3;
  wire [1:0]state;
  wire \state_reg_n_0_[0] ;
  wire \state_reg_n_0_[1] ;
  wire stop0;
  wire stop02_in;
  wire stop_i_10_n_0;
  wire stop_i_11_n_0;
  wire stop_i_13_n_0;
  wire stop_i_14_n_0;
  wire stop_i_15_n_0;
  wire stop_i_16_n_0;
  wire stop_i_18_n_0;
  wire stop_i_19_n_0;
  wire stop_i_1_n_0;
  wire stop_i_20_n_0;
  wire stop_i_21_n_0;
  wire stop_i_22_n_0;
  wire stop_i_23_n_0;
  wire stop_i_24_n_0;
  wire stop_i_25_n_0;
  wire stop_i_26_n_0;
  wire stop_i_27_n_0;
  wire stop_i_28_n_0;
  wire stop_i_29_n_0;
  wire stop_i_5_n_0;
  wire stop_i_6_n_0;
  wire stop_i_7_n_0;
  wire stop_i_9_n_0;
  wire stop_reg_i_12_n_0;
  wire stop_reg_i_12_n_1;
  wire stop_reg_i_12_n_2;
  wire stop_reg_i_12_n_3;
  wire stop_reg_i_17_n_0;
  wire stop_reg_i_17_n_1;
  wire stop_reg_i_17_n_2;
  wire stop_reg_i_17_n_3;
  wire stop_reg_i_2_n_2;
  wire stop_reg_i_2_n_3;
  wire stop_reg_i_3_n_2;
  wire stop_reg_i_3_n_3;
  wire stop_reg_i_4_n_0;
  wire stop_reg_i_4_n_1;
  wire stop_reg_i_4_n_2;
  wire stop_reg_i_4_n_3;
  wire stop_reg_i_8_n_0;
  wire stop_reg_i_8_n_1;
  wire stop_reg_i_8_n_2;
  wire stop_reg_i_8_n_3;
  wire stop_reg_n_0;
  wire uut4_n_2;
  wire uut4_n_3;
  wire uut4_n_4;
  wire uut5_n_2;
  wire [3:3]\NLW_count_reg[28]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_outp_reg[15]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_outp_reg[15]_i_3_O_UNCONNECTED ;
  wire [3:0]NLW_start_reg_i_12_O_UNCONNECTED;
  wire [3:2]NLW_start_reg_i_17_CO_UNCONNECTED;
  wire [3:3]NLW_start_reg_i_17_O_UNCONNECTED;
  wire [3:3]NLW_start_reg_i_2_CO_UNCONNECTED;
  wire [3:0]NLW_start_reg_i_2_O_UNCONNECTED;
  wire [3:0]NLW_start_reg_i_20_O_UNCONNECTED;
  wire [3:3]NLW_start_reg_i_3_CO_UNCONNECTED;
  wire [3:0]NLW_start_reg_i_3_O_UNCONNECTED;
  wire [3:0]NLW_start_reg_i_4_O_UNCONNECTED;
  wire [3:0]NLW_start_reg_i_8_O_UNCONNECTED;
  wire [3:0]NLW_stop_reg_i_12_O_UNCONNECTED;
  wire [3:0]NLW_stop_reg_i_17_O_UNCONNECTED;
  wire [3:3]NLW_stop_reg_i_2_CO_UNCONNECTED;
  wire [3:0]NLW_stop_reg_i_2_O_UNCONNECTED;
  wire [3:3]NLW_stop_reg_i_3_CO_UNCONNECTED;
  wire [3:0]NLW_stop_reg_i_3_O_UNCONNECTED;
  wire [3:0]NLW_stop_reg_i_4_O_UNCONNECTED;
  wire [3:0]NLW_stop_reg_i_8_O_UNCONNECTED;

  OBUF TxD_OBUF_inst
       (.I(TxD_OBUF),
        .O(TxD));
  BUFG clk_IBUF_BUFG_inst
       (.I(clk_IBUF),
        .O(clk_IBUF_BUFG));
  IBUF clk_IBUF_inst
       (.I(clk),
        .O(clk_IBUF));
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_2 
       (.I0(count_reg[0]),
        .O(\count[0]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[0]_i_1_n_7 ),
        .Q(count_reg[0]),
        .R(1'b0));
  CARRY4 \count_reg[0]_i_1 
       (.CI(1'b0),
        .CO({\count_reg[0]_i_1_n_0 ,\count_reg[0]_i_1_n_1 ,\count_reg[0]_i_1_n_2 ,\count_reg[0]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\count_reg[0]_i_1_n_4 ,\count_reg[0]_i_1_n_5 ,\count_reg[0]_i_1_n_6 ,\count_reg[0]_i_1_n_7 }),
        .S({count_reg[3:1],\count[0]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[8]_i_1_n_5 ),
        .Q(count_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[8]_i_1_n_4 ),
        .Q(count_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[12]_i_1_n_7 ),
        .Q(count_reg[12]),
        .R(1'b0));
  CARRY4 \count_reg[12]_i_1 
       (.CI(\count_reg[8]_i_1_n_0 ),
        .CO({\count_reg[12]_i_1_n_0 ,\count_reg[12]_i_1_n_1 ,\count_reg[12]_i_1_n_2 ,\count_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[12]_i_1_n_4 ,\count_reg[12]_i_1_n_5 ,\count_reg[12]_i_1_n_6 ,\count_reg[12]_i_1_n_7 }),
        .S(count_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[12]_i_1_n_6 ),
        .Q(count_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[12]_i_1_n_5 ),
        .Q(count_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[12]_i_1_n_4 ),
        .Q(count_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[16] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[16]_i_1_n_7 ),
        .Q(count_reg[16]),
        .R(1'b0));
  CARRY4 \count_reg[16]_i_1 
       (.CI(\count_reg[12]_i_1_n_0 ),
        .CO({\count_reg[16]_i_1_n_0 ,\count_reg[16]_i_1_n_1 ,\count_reg[16]_i_1_n_2 ,\count_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[16]_i_1_n_4 ,\count_reg[16]_i_1_n_5 ,\count_reg[16]_i_1_n_6 ,\count_reg[16]_i_1_n_7 }),
        .S(count_reg[19:16]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[17] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[16]_i_1_n_6 ),
        .Q(count_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[18] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[16]_i_1_n_5 ),
        .Q(count_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[19] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[16]_i_1_n_4 ),
        .Q(count_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[0]_i_1_n_6 ),
        .Q(count_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[20] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[20]_i_1_n_7 ),
        .Q(count_reg[20]),
        .R(1'b0));
  CARRY4 \count_reg[20]_i_1 
       (.CI(\count_reg[16]_i_1_n_0 ),
        .CO({\count_reg[20]_i_1_n_0 ,\count_reg[20]_i_1_n_1 ,\count_reg[20]_i_1_n_2 ,\count_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[20]_i_1_n_4 ,\count_reg[20]_i_1_n_5 ,\count_reg[20]_i_1_n_6 ,\count_reg[20]_i_1_n_7 }),
        .S(count_reg[23:20]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[21] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[20]_i_1_n_6 ),
        .Q(count_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[22] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[20]_i_1_n_5 ),
        .Q(count_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[23] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[20]_i_1_n_4 ),
        .Q(count_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[24] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[24]_i_1_n_7 ),
        .Q(count_reg[24]),
        .R(1'b0));
  CARRY4 \count_reg[24]_i_1 
       (.CI(\count_reg[20]_i_1_n_0 ),
        .CO({\count_reg[24]_i_1_n_0 ,\count_reg[24]_i_1_n_1 ,\count_reg[24]_i_1_n_2 ,\count_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[24]_i_1_n_4 ,\count_reg[24]_i_1_n_5 ,\count_reg[24]_i_1_n_6 ,\count_reg[24]_i_1_n_7 }),
        .S(count_reg[27:24]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[25] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[24]_i_1_n_6 ),
        .Q(count_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[26] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[24]_i_1_n_5 ),
        .Q(count_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[27] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[24]_i_1_n_4 ),
        .Q(count_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[28] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[28]_i_1_n_7 ),
        .Q(count_reg[28]),
        .R(1'b0));
  CARRY4 \count_reg[28]_i_1 
       (.CI(\count_reg[24]_i_1_n_0 ),
        .CO({\NLW_count_reg[28]_i_1_CO_UNCONNECTED [3],\count_reg[28]_i_1_n_1 ,\count_reg[28]_i_1_n_2 ,\count_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[28]_i_1_n_4 ,\count_reg[28]_i_1_n_5 ,\count_reg[28]_i_1_n_6 ,\count_reg[28]_i_1_n_7 }),
        .S(count_reg[31:28]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[29] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[28]_i_1_n_6 ),
        .Q(count_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[0]_i_1_n_5 ),
        .Q(count_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[30] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[28]_i_1_n_5 ),
        .Q(count_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[31] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[28]_i_1_n_4 ),
        .Q(count_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[0]_i_1_n_4 ),
        .Q(count_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[4]_i_1_n_7 ),
        .Q(count_reg[4]),
        .R(1'b0));
  CARRY4 \count_reg[4]_i_1 
       (.CI(\count_reg[0]_i_1_n_0 ),
        .CO({\count_reg[4]_i_1_n_0 ,\count_reg[4]_i_1_n_1 ,\count_reg[4]_i_1_n_2 ,\count_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[4]_i_1_n_4 ,\count_reg[4]_i_1_n_5 ,\count_reg[4]_i_1_n_6 ,\count_reg[4]_i_1_n_7 }),
        .S(count_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[4]_i_1_n_6 ),
        .Q(count_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[4]_i_1_n_5 ),
        .Q(count_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[4]_i_1_n_4 ),
        .Q(count_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[8]_i_1_n_7 ),
        .Q(count_reg[8]),
        .R(1'b0));
  CARRY4 \count_reg[8]_i_1 
       (.CI(\count_reg[4]_i_1_n_0 ),
        .CO({\count_reg[8]_i_1_n_0 ,\count_reg[8]_i_1_n_1 ,\count_reg[8]_i_1_n_2 ,\count_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_reg[8]_i_1_n_4 ,\count_reg[8]_i_1_n_5 ,\count_reg[8]_i_1_n_6 ,\count_reg[8]_i_1_n_7 }),
        .S(count_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \count_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\count_reg[8]_i_1_n_6 ),
        .Q(count_reg[9]),
        .R(1'b0));
  OBUF \latch_OBUF[0]_inst 
       (.I(latch_OBUF[0]),
        .O(latch[0]));
  OBUF \latch_OBUF[10]_inst 
       (.I(latch_OBUF[10]),
        .O(latch[10]));
  OBUF \latch_OBUF[11]_inst 
       (.I(latch_OBUF[11]),
        .O(latch[11]));
  OBUF \latch_OBUF[12]_inst 
       (.I(latch_OBUF[12]),
        .O(latch[12]));
  OBUF \latch_OBUF[13]_inst 
       (.I(latch_OBUF[13]),
        .O(latch[13]));
  OBUF \latch_OBUF[14]_inst 
       (.I(latch_OBUF[14]),
        .O(latch[14]));
  OBUF \latch_OBUF[15]_inst 
       (.I(latch_OBUF[15]),
        .O(latch[15]));
  OBUF \latch_OBUF[1]_inst 
       (.I(latch_OBUF[1]),
        .O(latch[1]));
  OBUF \latch_OBUF[2]_inst 
       (.I(latch_OBUF[2]),
        .O(latch[2]));
  OBUF \latch_OBUF[3]_inst 
       (.I(latch_OBUF[3]),
        .O(latch[3]));
  OBUF \latch_OBUF[4]_inst 
       (.I(latch_OBUF[4]),
        .O(latch[4]));
  OBUF \latch_OBUF[5]_inst 
       (.I(latch_OBUF[5]),
        .O(latch[5]));
  OBUF \latch_OBUF[6]_inst 
       (.I(latch_OBUF[6]),
        .O(latch[6]));
  OBUF \latch_OBUF[7]_inst 
       (.I(latch_OBUF[7]),
        .O(latch[7]));
  OBUF \latch_OBUF[8]_inst 
       (.I(latch_OBUF[8]),
        .O(latch[8]));
  OBUF \latch_OBUF[9]_inst 
       (.I(latch_OBUF[9]),
        .O(latch[9]));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[0]),
        .Q(latch_OBUF[0]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[10]),
        .Q(latch_OBUF[10]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[11]),
        .Q(latch_OBUF[11]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[12]),
        .Q(latch_OBUF[12]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[13]),
        .Q(latch_OBUF[13]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[14]),
        .Q(latch_OBUF[14]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[15]),
        .Q(latch_OBUF[15]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[1]),
        .Q(latch_OBUF[1]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[2]),
        .Q(latch_OBUF[2]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[3]),
        .Q(latch_OBUF[3]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[4]),
        .Q(latch_OBUF[4]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[5]),
        .Q(latch_OBUF[5]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[6]),
        .Q(latch_OBUF[6]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[7]),
        .Q(latch_OBUF[7]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[8]),
        .Q(latch_OBUF[8]),
        .R(uut4_n_4));
  FDRE #(
    .INIT(1'b0)) 
    \latch_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(uut4_n_2),
        .D(outp[9]),
        .Q(latch_OBUF[9]),
        .R(uut4_n_4));
  LUT1 #(
    .INIT(2'h1)) 
    \outp[0]_i_1 
       (.I0(outp[0]),
        .O(\outp[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \outp[15]_i_1 
       (.I0(\state_reg_n_0_[1] ),
        .I1(\state_reg_n_0_[0] ),
        .O(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(\outp[0]_i_1_n_0 ),
        .Q(outp[0]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[10]),
        .Q(outp[10]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[11]),
        .Q(outp[11]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[12]),
        .Q(outp[12]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[12]_i_1 
       (.CI(\outp_reg[8]_i_1_n_0 ),
        .CO({\outp_reg[12]_i_1_n_0 ,\outp_reg[12]_i_1_n_1 ,\outp_reg[12]_i_1_n_2 ,\outp_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(outp0[12:9]),
        .S(outp[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[13]),
        .Q(outp[13]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[14] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[14]),
        .Q(outp[14]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[15] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[15]),
        .Q(outp[15]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[15]_i_3 
       (.CI(\outp_reg[12]_i_1_n_0 ),
        .CO({\NLW_outp_reg[15]_i_3_CO_UNCONNECTED [3:2],\outp_reg[15]_i_3_n_2 ,\outp_reg[15]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_outp_reg[15]_i_3_O_UNCONNECTED [3],outp0[15:13]}),
        .S({1'b0,outp[15:13]}));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[1]),
        .Q(outp[1]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[2]),
        .Q(outp[2]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[3]),
        .Q(outp[3]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[4]),
        .Q(outp[4]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\outp_reg[4]_i_1_n_0 ,\outp_reg[4]_i_1_n_1 ,\outp_reg[4]_i_1_n_2 ,\outp_reg[4]_i_1_n_3 }),
        .CYINIT(outp[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(outp0[4:1]),
        .S(outp[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[5]),
        .Q(outp[5]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[6]),
        .Q(outp[6]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[7]),
        .Q(outp[7]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[8]),
        .Q(outp[8]),
        .R(\outp[15]_i_1_n_0 ));
  CARRY4 \outp_reg[8]_i_1 
       (.CI(\outp_reg[4]_i_1_n_0 ),
        .CO({\outp_reg[8]_i_1_n_0 ,\outp_reg[8]_i_1_n_1 ,\outp_reg[8]_i_1_n_2 ,\outp_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(outp0[8:5]),
        .S(outp[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \outp_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(uut5_n_2),
        .D(outp0[9]),
        .Q(outp[9]),
        .R(\outp[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    reset_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(uut4_n_3),
        .Q(reset),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hF4)) 
    start_i_1
       (.I0(start_reg_i_2_n_1),
        .I1(start),
        .I2(start_reg_i_3_n_1),
        .O(start_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_10
       (.I0(start_reg_i_17_n_7),
        .I1(start_reg_i_18_n_4),
        .I2(start_reg_i_18_n_5),
        .O(start_i_10_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_11
       (.I0(start_reg_i_18_n_6),
        .I1(start_reg_i_18_n_7),
        .I2(start_reg_i_19_n_4),
        .O(start_i_11_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_13
       (.I0(start_reg_i_19_n_5),
        .I1(start_reg_i_19_n_6),
        .I2(start_reg_i_19_n_7),
        .O(start_i_13_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_14
       (.I0(start_reg_i_29_n_4),
        .I1(start_reg_i_29_n_5),
        .I2(start_reg_i_29_n_6),
        .O(start_i_14_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_15
       (.I0(start_reg_i_29_n_7),
        .I1(start_reg_i_30_n_4),
        .I2(start_reg_i_30_n_5),
        .O(start_i_15_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_16
       (.I0(start_reg_i_30_n_6),
        .I1(start_reg_i_30_n_7),
        .I2(start_reg_i_31_n_4),
        .O(start_i_16_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_21
       (.I0(start_reg_i_19_n_5),
        .I1(start_reg_i_19_n_6),
        .I2(start_reg_i_19_n_7),
        .O(start_i_21_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_22
       (.I0(start_reg_i_29_n_4),
        .I1(start_reg_i_29_n_5),
        .I2(start_reg_i_29_n_6),
        .O(start_i_22_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_23
       (.I0(start_reg_i_29_n_7),
        .I1(start_reg_i_30_n_4),
        .I2(start_reg_i_30_n_5),
        .O(start_i_23_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_24
       (.I0(start_reg_i_30_n_6),
        .I1(start_reg_i_30_n_7),
        .I2(start_reg_i_31_n_4),
        .O(start_i_24_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_25
       (.I0(start_reg_i_31_n_5),
        .I1(start_reg_i_31_n_6),
        .I2(start_reg_i_31_n_7),
        .O(start_i_25_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_26
       (.I0(start_reg_i_36_n_4),
        .I1(start_reg_i_36_n_5),
        .I2(start_reg_i_36_n_6),
        .O(start_i_26_n_0));
  LUT3 #(
    .INIT(8'h10)) 
    start_i_27
       (.I0(start_reg_i_36_n_7),
        .I1(start_reg_i_37_n_4),
        .I2(start_reg_i_37_n_5),
        .O(start_i_27_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    start_i_28
       (.I0(start_reg_i_37_n_6),
        .I1(start_reg_i_37_n_7),
        .I2(count_reg[0]),
        .O(start_i_28_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_32
       (.I0(start_reg_i_31_n_5),
        .I1(start_reg_i_31_n_6),
        .I2(start_reg_i_31_n_7),
        .O(start_i_32_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_33
       (.I0(start_reg_i_36_n_4),
        .I1(start_reg_i_36_n_5),
        .I2(start_reg_i_36_n_6),
        .O(start_i_33_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_34
       (.I0(start_reg_i_36_n_7),
        .I1(start_reg_i_37_n_4),
        .I2(start_reg_i_37_n_5),
        .O(start_i_34_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    start_i_35
       (.I0(start_reg_i_37_n_7),
        .I1(start_reg_i_37_n_6),
        .I2(count_reg[0]),
        .O(start_i_35_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    start_i_5
       (.I0(start_reg_i_17_n_6),
        .I1(start_reg_i_17_n_5),
        .O(start_i_5_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_6
       (.I0(start_reg_i_17_n_7),
        .I1(start_reg_i_18_n_4),
        .I2(start_reg_i_18_n_5),
        .O(start_i_6_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    start_i_7
       (.I0(start_reg_i_18_n_6),
        .I1(start_reg_i_18_n_7),
        .I2(start_reg_i_19_n_4),
        .O(start_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    start_i_9
       (.I0(start_reg_i_17_n_6),
        .I1(start_reg_i_17_n_5),
        .O(start_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    start_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(start_i_1_n_0),
        .Q(start),
        .R(1'b0));
  CARRY4 start_reg_i_12
       (.CI(1'b0),
        .CO({start_reg_i_12_n_0,start_reg_i_12_n_1,start_reg_i_12_n_2,start_reg_i_12_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_reg_i_12_O_UNCONNECTED[3:0]),
        .S({start_i_25_n_0,start_i_26_n_0,start_i_27_n_0,start_i_28_n_0}));
  CARRY4 start_reg_i_17
       (.CI(start_reg_i_18_n_0),
        .CO({NLW_start_reg_i_17_CO_UNCONNECTED[3:2],start_reg_i_17_n_2,start_reg_i_17_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_start_reg_i_17_O_UNCONNECTED[3],start_reg_i_17_n_5,start_reg_i_17_n_6,start_reg_i_17_n_7}),
        .S({1'b0,count_reg[31:29]}));
  CARRY4 start_reg_i_18
       (.CI(start_reg_i_19_n_0),
        .CO({start_reg_i_18_n_0,start_reg_i_18_n_1,start_reg_i_18_n_2,start_reg_i_18_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_18_n_4,start_reg_i_18_n_5,start_reg_i_18_n_6,start_reg_i_18_n_7}),
        .S(count_reg[28:25]));
  CARRY4 start_reg_i_19
       (.CI(start_reg_i_29_n_0),
        .CO({start_reg_i_19_n_0,start_reg_i_19_n_1,start_reg_i_19_n_2,start_reg_i_19_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_19_n_4,start_reg_i_19_n_5,start_reg_i_19_n_6,start_reg_i_19_n_7}),
        .S(count_reg[24:21]));
  CARRY4 start_reg_i_2
       (.CI(start_reg_i_4_n_0),
        .CO({NLW_start_reg_i_2_CO_UNCONNECTED[3],start_reg_i_2_n_1,start_reg_i_2_n_2,start_reg_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_reg_i_2_O_UNCONNECTED[3:0]),
        .S({1'b0,start_i_5_n_0,start_i_6_n_0,start_i_7_n_0}));
  CARRY4 start_reg_i_20
       (.CI(1'b0),
        .CO({start_reg_i_20_n_0,start_reg_i_20_n_1,start_reg_i_20_n_2,start_reg_i_20_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_reg_i_20_O_UNCONNECTED[3:0]),
        .S({start_i_32_n_0,start_i_33_n_0,start_i_34_n_0,start_i_35_n_0}));
  CARRY4 start_reg_i_29
       (.CI(start_reg_i_30_n_0),
        .CO({start_reg_i_29_n_0,start_reg_i_29_n_1,start_reg_i_29_n_2,start_reg_i_29_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_29_n_4,start_reg_i_29_n_5,start_reg_i_29_n_6,start_reg_i_29_n_7}),
        .S(count_reg[20:17]));
  CARRY4 start_reg_i_3
       (.CI(start_reg_i_8_n_0),
        .CO({NLW_start_reg_i_3_CO_UNCONNECTED[3],start_reg_i_3_n_1,start_reg_i_3_n_2,start_reg_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_reg_i_3_O_UNCONNECTED[3:0]),
        .S({1'b0,start_i_9_n_0,start_i_10_n_0,start_i_11_n_0}));
  CARRY4 start_reg_i_30
       (.CI(start_reg_i_31_n_0),
        .CO({start_reg_i_30_n_0,start_reg_i_30_n_1,start_reg_i_30_n_2,start_reg_i_30_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_30_n_4,start_reg_i_30_n_5,start_reg_i_30_n_6,start_reg_i_30_n_7}),
        .S(count_reg[16:13]));
  CARRY4 start_reg_i_31
       (.CI(start_reg_i_36_n_0),
        .CO({start_reg_i_31_n_0,start_reg_i_31_n_1,start_reg_i_31_n_2,start_reg_i_31_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_31_n_4,start_reg_i_31_n_5,start_reg_i_31_n_6,start_reg_i_31_n_7}),
        .S(count_reg[12:9]));
  CARRY4 start_reg_i_36
       (.CI(start_reg_i_37_n_0),
        .CO({start_reg_i_36_n_0,start_reg_i_36_n_1,start_reg_i_36_n_2,start_reg_i_36_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_36_n_4,start_reg_i_36_n_5,start_reg_i_36_n_6,start_reg_i_36_n_7}),
        .S(count_reg[8:5]));
  CARRY4 start_reg_i_37
       (.CI(1'b0),
        .CO({start_reg_i_37_n_0,start_reg_i_37_n_1,start_reg_i_37_n_2,start_reg_i_37_n_3}),
        .CYINIT(count_reg[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({start_reg_i_37_n_4,start_reg_i_37_n_5,start_reg_i_37_n_6,start_reg_i_37_n_7}),
        .S(count_reg[4:1]));
  CARRY4 start_reg_i_4
       (.CI(start_reg_i_12_n_0),
        .CO({start_reg_i_4_n_0,start_reg_i_4_n_1,start_reg_i_4_n_2,start_reg_i_4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_reg_i_4_O_UNCONNECTED[3:0]),
        .S({start_i_13_n_0,start_i_14_n_0,start_i_15_n_0,start_i_16_n_0}));
  CARRY4 start_reg_i_8
       (.CI(start_reg_i_20_n_0),
        .CO({start_reg_i_8_n_0,start_reg_i_8_n_1,start_reg_i_8_n_2,start_reg_i_8_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_start_reg_i_8_O_UNCONNECTED[3:0]),
        .S({start_i_21_n_0,start_i_22_n_0,start_i_23_n_0,start_i_24_n_0}));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(state[0]),
        .Q(\state_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \state_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(state[1]),
        .Q(\state_reg_n_0_[1] ),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFEFF0202)) 
    stop_i_1
       (.I0(stop0),
        .I1(start_reg_i_2_n_1),
        .I2(start_reg_i_3_n_1),
        .I3(stop02_in),
        .I4(stop_reg_n_0),
        .O(stop_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_10
       (.I0(start_reg_i_17_n_7),
        .I1(start_reg_i_18_n_4),
        .I2(start_reg_i_18_n_5),
        .O(stop_i_10_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_11
       (.I0(start_reg_i_18_n_6),
        .I1(start_reg_i_18_n_7),
        .I2(start_reg_i_19_n_4),
        .O(stop_i_11_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_13
       (.I0(start_reg_i_19_n_5),
        .I1(start_reg_i_19_n_6),
        .I2(start_reg_i_19_n_7),
        .O(stop_i_13_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_14
       (.I0(start_reg_i_29_n_4),
        .I1(start_reg_i_29_n_5),
        .I2(start_reg_i_29_n_6),
        .O(stop_i_14_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_15
       (.I0(start_reg_i_29_n_7),
        .I1(start_reg_i_30_n_4),
        .I2(start_reg_i_30_n_5),
        .O(stop_i_15_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_16
       (.I0(start_reg_i_30_n_6),
        .I1(start_reg_i_30_n_7),
        .I2(start_reg_i_31_n_4),
        .O(stop_i_16_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_18
       (.I0(start_reg_i_19_n_5),
        .I1(start_reg_i_19_n_6),
        .I2(start_reg_i_19_n_7),
        .O(stop_i_18_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_19
       (.I0(start_reg_i_29_n_4),
        .I1(start_reg_i_29_n_5),
        .I2(start_reg_i_29_n_6),
        .O(stop_i_19_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_20
       (.I0(start_reg_i_29_n_7),
        .I1(start_reg_i_30_n_4),
        .I2(start_reg_i_30_n_5),
        .O(stop_i_20_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_21
       (.I0(start_reg_i_30_n_6),
        .I1(start_reg_i_30_n_7),
        .I2(start_reg_i_31_n_4),
        .O(stop_i_21_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_22
       (.I0(start_reg_i_31_n_5),
        .I1(start_reg_i_31_n_6),
        .I2(start_reg_i_31_n_7),
        .O(stop_i_22_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_23
       (.I0(start_reg_i_36_n_4),
        .I1(start_reg_i_36_n_5),
        .I2(start_reg_i_36_n_6),
        .O(stop_i_23_n_0));
  LUT3 #(
    .INIT(8'h10)) 
    stop_i_24
       (.I0(start_reg_i_36_n_7),
        .I1(start_reg_i_37_n_4),
        .I2(start_reg_i_37_n_5),
        .O(stop_i_24_n_0));
  LUT3 #(
    .INIT(8'h08)) 
    stop_i_25
       (.I0(start_reg_i_37_n_6),
        .I1(start_reg_i_37_n_7),
        .I2(count_reg[0]),
        .O(stop_i_25_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_26
       (.I0(start_reg_i_31_n_5),
        .I1(start_reg_i_31_n_6),
        .I2(start_reg_i_31_n_7),
        .O(stop_i_26_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_27
       (.I0(start_reg_i_36_n_4),
        .I1(start_reg_i_36_n_5),
        .I2(start_reg_i_36_n_6),
        .O(stop_i_27_n_0));
  LUT3 #(
    .INIT(8'h04)) 
    stop_i_28
       (.I0(start_reg_i_36_n_7),
        .I1(start_reg_i_37_n_4),
        .I2(start_reg_i_37_n_5),
        .O(stop_i_28_n_0));
  LUT3 #(
    .INIT(8'h40)) 
    stop_i_29
       (.I0(start_reg_i_37_n_7),
        .I1(start_reg_i_37_n_6),
        .I2(count_reg[0]),
        .O(stop_i_29_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    stop_i_5
       (.I0(start_reg_i_17_n_6),
        .I1(start_reg_i_17_n_5),
        .O(stop_i_5_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_6
       (.I0(start_reg_i_17_n_7),
        .I1(start_reg_i_18_n_4),
        .I2(start_reg_i_18_n_5),
        .O(stop_i_6_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    stop_i_7
       (.I0(start_reg_i_18_n_6),
        .I1(start_reg_i_18_n_7),
        .I2(start_reg_i_19_n_4),
        .O(stop_i_7_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    stop_i_9
       (.I0(start_reg_i_17_n_6),
        .I1(start_reg_i_17_n_5),
        .O(stop_i_9_n_0));
  FDRE #(
    .INIT(1'b0)) 
    stop_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(stop_i_1_n_0),
        .Q(stop_reg_n_0),
        .R(1'b0));
  CARRY4 stop_reg_i_12
       (.CI(1'b0),
        .CO({stop_reg_i_12_n_0,stop_reg_i_12_n_1,stop_reg_i_12_n_2,stop_reg_i_12_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_stop_reg_i_12_O_UNCONNECTED[3:0]),
        .S({stop_i_22_n_0,stop_i_23_n_0,stop_i_24_n_0,stop_i_25_n_0}));
  CARRY4 stop_reg_i_17
       (.CI(1'b0),
        .CO({stop_reg_i_17_n_0,stop_reg_i_17_n_1,stop_reg_i_17_n_2,stop_reg_i_17_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_stop_reg_i_17_O_UNCONNECTED[3:0]),
        .S({stop_i_26_n_0,stop_i_27_n_0,stop_i_28_n_0,stop_i_29_n_0}));
  CARRY4 stop_reg_i_2
       (.CI(stop_reg_i_4_n_0),
        .CO({NLW_stop_reg_i_2_CO_UNCONNECTED[3],stop0,stop_reg_i_2_n_2,stop_reg_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_stop_reg_i_2_O_UNCONNECTED[3:0]),
        .S({1'b0,stop_i_5_n_0,stop_i_6_n_0,stop_i_7_n_0}));
  CARRY4 stop_reg_i_3
       (.CI(stop_reg_i_8_n_0),
        .CO({NLW_stop_reg_i_3_CO_UNCONNECTED[3],stop02_in,stop_reg_i_3_n_2,stop_reg_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_stop_reg_i_3_O_UNCONNECTED[3:0]),
        .S({1'b0,stop_i_9_n_0,stop_i_10_n_0,stop_i_11_n_0}));
  CARRY4 stop_reg_i_4
       (.CI(stop_reg_i_12_n_0),
        .CO({stop_reg_i_4_n_0,stop_reg_i_4_n_1,stop_reg_i_4_n_2,stop_reg_i_4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_stop_reg_i_4_O_UNCONNECTED[3:0]),
        .S({stop_i_13_n_0,stop_i_14_n_0,stop_i_15_n_0,stop_i_16_n_0}));
  CARRY4 stop_reg_i_8
       (.CI(stop_reg_i_17_n_0),
        .CO({stop_reg_i_8_n_0,stop_reg_i_8_n_1,stop_reg_i_8_n_2,stop_reg_i_8_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_stop_reg_i_8_O_UNCONNECTED[3:0]),
        .S({stop_i_18_n_0,stop_i_19_n_0,stop_i_20_n_0,stop_i_21_n_0}));
  transmitter uut
       (.TxD_OBUF(TxD_OBUF),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .latch_OBUF(latch_OBUF[7:0]),
        .start(start));
  DFF uut4
       (.D(state[0]),
        .Q({\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .T0(T0),
        .T4(T4),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\latch_reg[0] (uut4_n_2),
        .\latch_reg[15] (uut4_n_4),
        .reset(reset),
        .reset_reg(uut4_n_3),
        .start(start));
  DFF_0 uut5
       (.D(state[1]),
        .Q({\state_reg_n_0_[1] ,\state_reg_n_0_[0] }),
        .T0(T0),
        .T4(T4),
        .clk_IBUF_BUFG(clk_IBUF_BUFG),
        .\outp_reg[0] (uut5_n_2),
        .stop_reg(stop_reg_n_0));
endmodule

module transmitter
   (TxD_OBUF,
    clk_IBUF_BUFG,
    start,
    latch_OBUF);
  output TxD_OBUF;
  input clk_IBUF_BUFG;
  input start;
  input [7:0]latch_OBUF;

  wire TxD_OBUF;
  wire TxD_i_1_n_0;
  wire bitcounter;
  wire \bitcounter[3]_i_1_n_0 ;
  wire \bitcounter[3]_i_4_n_0 ;
  wire \bitcounter[3]_i_5_n_0 ;
  wire [3:1]bitcounter_reg__0;
  wire \bitcounter_reg_n_0_[0] ;
  wire clear;
  wire clear_i_1_n_0;
  wire clk_IBUF_BUFG;
  wire \counter[0]_i_1_n_0 ;
  wire \counter[0]_i_3_n_0 ;
  wire [13:0]counter_reg;
  wire \counter_reg[0]_i_2_n_0 ;
  wire \counter_reg[0]_i_2_n_1 ;
  wire \counter_reg[0]_i_2_n_2 ;
  wire \counter_reg[0]_i_2_n_3 ;
  wire \counter_reg[0]_i_2_n_4 ;
  wire \counter_reg[0]_i_2_n_5 ;
  wire \counter_reg[0]_i_2_n_6 ;
  wire \counter_reg[0]_i_2_n_7 ;
  wire \counter_reg[12]_i_1_n_3 ;
  wire \counter_reg[12]_i_1_n_6 ;
  wire \counter_reg[12]_i_1_n_7 ;
  wire \counter_reg[4]_i_1_n_0 ;
  wire \counter_reg[4]_i_1_n_1 ;
  wire \counter_reg[4]_i_1_n_2 ;
  wire \counter_reg[4]_i_1_n_3 ;
  wire \counter_reg[4]_i_1_n_4 ;
  wire \counter_reg[4]_i_1_n_5 ;
  wire \counter_reg[4]_i_1_n_6 ;
  wire \counter_reg[4]_i_1_n_7 ;
  wire \counter_reg[8]_i_1_n_0 ;
  wire \counter_reg[8]_i_1_n_1 ;
  wire \counter_reg[8]_i_1_n_2 ;
  wire \counter_reg[8]_i_1_n_3 ;
  wire \counter_reg[8]_i_1_n_4 ;
  wire \counter_reg[8]_i_1_n_5 ;
  wire \counter_reg[8]_i_1_n_6 ;
  wire \counter_reg[8]_i_1_n_7 ;
  wire [7:0]latch_OBUF;
  wire load;
  wire nextstate;
  wire nextstate_0;
  wire [3:0]p_0_in;
  wire rightshiftreg;
  wire \rightshiftreg[0]_i_1_n_0 ;
  wire \rightshiftreg[1]_i_1_n_0 ;
  wire \rightshiftreg[2]_i_1_n_0 ;
  wire \rightshiftreg[3]_i_1_n_0 ;
  wire \rightshiftreg[4]_i_1_n_0 ;
  wire \rightshiftreg[5]_i_1_n_0 ;
  wire \rightshiftreg[6]_i_1_n_0 ;
  wire \rightshiftreg[7]_i_1_n_0 ;
  wire \rightshiftreg[8]_i_1_n_0 ;
  wire \rightshiftreg[9]_i_2_n_0 ;
  wire \rightshiftreg[9]_i_3_n_0 ;
  wire \rightshiftreg[9]_i_4_n_0 ;
  wire \rightshiftreg[9]_i_5_n_0 ;
  wire \rightshiftreg_reg_n_0_[0] ;
  wire \rightshiftreg_reg_n_0_[1] ;
  wire \rightshiftreg_reg_n_0_[2] ;
  wire \rightshiftreg_reg_n_0_[3] ;
  wire \rightshiftreg_reg_n_0_[4] ;
  wire \rightshiftreg_reg_n_0_[5] ;
  wire \rightshiftreg_reg_n_0_[6] ;
  wire \rightshiftreg_reg_n_0_[7] ;
  wire \rightshiftreg_reg_n_0_[8] ;
  wire \rightshiftreg_reg_n_0_[9] ;
  wire shift;
  wire shift_i_2_n_0;
  wire start;
  wire state;
  wire state_i_1_n_0;
  wire [3:1]\NLW_counter_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_counter_reg[12]_i_1_O_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hA8FF)) 
    TxD_i_1
       (.I0(bitcounter_reg__0[3]),
        .I1(bitcounter_reg__0[1]),
        .I2(bitcounter_reg__0[2]),
        .I3(state),
        .O(TxD_i_1_n_0));
  FDSE #(
    .INIT(1'b1)) 
    TxD_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\rightshiftreg_reg_n_0_[0] ),
        .Q(TxD_OBUF),
        .S(TxD_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \bitcounter[0]_i_1 
       (.I0(\bitcounter_reg_n_0_[0] ),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \bitcounter[1]_i_1 
       (.I0(\bitcounter_reg_n_0_[0] ),
        .I1(bitcounter_reg__0[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \bitcounter[2]_i_1 
       (.I0(bitcounter_reg__0[1]),
        .I1(\bitcounter_reg_n_0_[0] ),
        .I2(bitcounter_reg__0[2]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'hFFFFFFFF0000E000)) 
    \bitcounter[3]_i_1 
       (.I0(counter_reg[12]),
        .I1(\bitcounter[3]_i_4_n_0 ),
        .I2(counter_reg[13]),
        .I3(clear),
        .I4(shift),
        .I5(start),
        .O(\bitcounter[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAA800000)) 
    \bitcounter[3]_i_2 
       (.I0(counter_reg[13]),
        .I1(counter_reg[11]),
        .I2(\rightshiftreg[9]_i_4_n_0 ),
        .I3(counter_reg[12]),
        .I4(shift),
        .O(bitcounter));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \bitcounter[3]_i_3 
       (.I0(bitcounter_reg__0[2]),
        .I1(\bitcounter_reg_n_0_[0] ),
        .I2(bitcounter_reg__0[1]),
        .I3(bitcounter_reg__0[3]),
        .O(p_0_in[3]));
  LUT6 #(
    .INIT(64'hFFFFFFA800000000)) 
    \bitcounter[3]_i_4 
       (.I0(counter_reg[7]),
        .I1(\rightshiftreg[9]_i_5_n_0 ),
        .I2(counter_reg[6]),
        .I3(counter_reg[10]),
        .I4(\bitcounter[3]_i_5_n_0 ),
        .I5(counter_reg[11]),
        .O(\bitcounter[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \bitcounter[3]_i_5 
       (.I0(counter_reg[8]),
        .I1(counter_reg[9]),
        .O(\bitcounter[3]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitcounter_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(bitcounter),
        .D(p_0_in[0]),
        .Q(\bitcounter_reg_n_0_[0] ),
        .R(\bitcounter[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitcounter_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(bitcounter),
        .D(p_0_in[1]),
        .Q(bitcounter_reg__0[1]),
        .R(\bitcounter[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitcounter_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(bitcounter),
        .D(p_0_in[2]),
        .Q(bitcounter_reg__0[2]),
        .R(\bitcounter[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \bitcounter_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(bitcounter),
        .D(p_0_in[3]),
        .Q(bitcounter_reg__0[3]),
        .R(\bitcounter[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hA8)) 
    clear_i_1
       (.I0(bitcounter_reg__0[3]),
        .I1(bitcounter_reg__0[1]),
        .I2(bitcounter_reg__0[2]),
        .O(clear_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    clear_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(clear_i_1_n_0),
        .Q(clear),
        .R(nextstate_0));
  LUT5 #(
    .INIT(32'hFFFFAA80)) 
    \counter[0]_i_1 
       (.I0(counter_reg[13]),
        .I1(counter_reg[11]),
        .I2(\rightshiftreg[9]_i_4_n_0 ),
        .I3(counter_reg[12]),
        .I4(start),
        .O(\counter[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \counter[0]_i_3 
       (.I0(counter_reg[0]),
        .O(\counter[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[0]_i_2_n_7 ),
        .Q(counter_reg[0]),
        .R(\counter[0]_i_1_n_0 ));
  CARRY4 \counter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\counter_reg[0]_i_2_n_0 ,\counter_reg[0]_i_2_n_1 ,\counter_reg[0]_i_2_n_2 ,\counter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counter_reg[0]_i_2_n_4 ,\counter_reg[0]_i_2_n_5 ,\counter_reg[0]_i_2_n_6 ,\counter_reg[0]_i_2_n_7 }),
        .S({counter_reg[3:1],\counter[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[10] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[8]_i_1_n_5 ),
        .Q(counter_reg[10]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[11] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[8]_i_1_n_4 ),
        .Q(counter_reg[11]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[12] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[12]_i_1_n_7 ),
        .Q(counter_reg[12]),
        .R(\counter[0]_i_1_n_0 ));
  CARRY4 \counter_reg[12]_i_1 
       (.CI(\counter_reg[8]_i_1_n_0 ),
        .CO({\NLW_counter_reg[12]_i_1_CO_UNCONNECTED [3:1],\counter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_counter_reg[12]_i_1_O_UNCONNECTED [3:2],\counter_reg[12]_i_1_n_6 ,\counter_reg[12]_i_1_n_7 }),
        .S({1'b0,1'b0,counter_reg[13:12]}));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[13] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[12]_i_1_n_6 ),
        .Q(counter_reg[13]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[0]_i_2_n_6 ),
        .Q(counter_reg[1]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[0]_i_2_n_5 ),
        .Q(counter_reg[2]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[0]_i_2_n_4 ),
        .Q(counter_reg[3]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[4]_i_1_n_7 ),
        .Q(counter_reg[4]),
        .R(\counter[0]_i_1_n_0 ));
  CARRY4 \counter_reg[4]_i_1 
       (.CI(\counter_reg[0]_i_2_n_0 ),
        .CO({\counter_reg[4]_i_1_n_0 ,\counter_reg[4]_i_1_n_1 ,\counter_reg[4]_i_1_n_2 ,\counter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[4]_i_1_n_4 ,\counter_reg[4]_i_1_n_5 ,\counter_reg[4]_i_1_n_6 ,\counter_reg[4]_i_1_n_7 }),
        .S(counter_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[4]_i_1_n_6 ),
        .Q(counter_reg[5]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[4]_i_1_n_5 ),
        .Q(counter_reg[6]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[4]_i_1_n_4 ),
        .Q(counter_reg[7]),
        .R(\counter[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[8]_i_1_n_7 ),
        .Q(counter_reg[8]),
        .R(\counter[0]_i_1_n_0 ));
  CARRY4 \counter_reg[8]_i_1 
       (.CI(\counter_reg[4]_i_1_n_0 ),
        .CO({\counter_reg[8]_i_1_n_0 ,\counter_reg[8]_i_1_n_1 ,\counter_reg[8]_i_1_n_2 ,\counter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counter_reg[8]_i_1_n_4 ,\counter_reg[8]_i_1_n_5 ,\counter_reg[8]_i_1_n_6 ,\counter_reg[8]_i_1_n_7 }),
        .S(counter_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \counter_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(\counter_reg[8]_i_1_n_6 ),
        .Q(counter_reg[9]),
        .R(\counter[0]_i_1_n_0 ));
  FDSE #(
    .INIT(1'b1)) 
    load_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(1'b0),
        .Q(load),
        .S(nextstate_0));
  FDSE #(
    .INIT(1'b1)) 
    nextstate_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(shift_i_2_n_0),
        .Q(nextstate),
        .S(nextstate_0));
  LUT2 #(
    .INIT(4'h8)) 
    \rightshiftreg[0]_i_1 
       (.I0(shift),
        .I1(\rightshiftreg_reg_n_0_[1] ),
        .O(\rightshiftreg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[1]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[2] ),
        .I1(shift),
        .I2(latch_OBUF[0]),
        .O(\rightshiftreg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[2]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[3] ),
        .I1(shift),
        .I2(latch_OBUF[1]),
        .O(\rightshiftreg[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[3]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[4] ),
        .I1(shift),
        .I2(latch_OBUF[2]),
        .O(\rightshiftreg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[4]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[5] ),
        .I1(shift),
        .I2(latch_OBUF[3]),
        .O(\rightshiftreg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[5]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[6] ),
        .I1(shift),
        .I2(latch_OBUF[4]),
        .O(\rightshiftreg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[6]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[7] ),
        .I1(shift),
        .I2(latch_OBUF[5]),
        .O(\rightshiftreg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[7]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[8] ),
        .I1(shift),
        .I2(latch_OBUF[6]),
        .O(\rightshiftreg[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \rightshiftreg[8]_i_1 
       (.I0(\rightshiftreg_reg_n_0_[9] ),
        .I1(shift),
        .I2(latch_OBUF[7]),
        .O(\rightshiftreg[8]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h88888000)) 
    \rightshiftreg[9]_i_1 
       (.I0(\rightshiftreg[9]_i_3_n_0 ),
        .I1(counter_reg[13]),
        .I2(counter_reg[11]),
        .I3(\rightshiftreg[9]_i_4_n_0 ),
        .I4(counter_reg[12]),
        .O(rightshiftreg));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \rightshiftreg[9]_i_2 
       (.I0(shift),
        .O(\rightshiftreg[9]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    \rightshiftreg[9]_i_3 
       (.I0(shift),
        .I1(load),
        .I2(start),
        .O(\rightshiftreg[9]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFEFEFEFE)) 
    \rightshiftreg[9]_i_4 
       (.I0(counter_reg[9]),
        .I1(counter_reg[8]),
        .I2(counter_reg[10]),
        .I3(counter_reg[6]),
        .I4(\rightshiftreg[9]_i_5_n_0 ),
        .I5(counter_reg[7]),
        .O(\rightshiftreg[9]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hEAAAAAAA00000000)) 
    \rightshiftreg[9]_i_5 
       (.I0(counter_reg[4]),
        .I1(counter_reg[1]),
        .I2(counter_reg[0]),
        .I3(counter_reg[3]),
        .I4(counter_reg[2]),
        .I5(counter_reg[5]),
        .O(\rightshiftreg[9]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[0] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[0]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[1] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[1]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[2] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[2]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[3] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[3]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[4] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[4]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[5] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[5]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[6] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[6]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[7] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[7]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[8] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[8]_i_1_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rightshiftreg_reg[9] 
       (.C(clk_IBUF_BUFG),
        .CE(rightshiftreg),
        .D(\rightshiftreg[9]_i_2_n_0 ),
        .Q(\rightshiftreg_reg_n_0_[9] ),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h1)) 
    shift_i_1
       (.I0(state),
        .O(nextstate_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h1F)) 
    shift_i_2
       (.I0(bitcounter_reg__0[2]),
        .I1(bitcounter_reg__0[1]),
        .I2(bitcounter_reg__0[3]),
        .O(shift_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    shift_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(shift_i_2_n_0),
        .Q(shift),
        .R(nextstate_0));
  LUT6 #(
    .INIT(64'h00000000EEEA222A)) 
    state_i_1
       (.I0(state),
        .I1(counter_reg[13]),
        .I2(\bitcounter[3]_i_4_n_0 ),
        .I3(counter_reg[12]),
        .I4(nextstate),
        .I5(start),
        .O(state_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    state_reg
       (.C(clk_IBUF_BUFG),
        .CE(1'b1),
        .D(state_i_1_n_0),
        .Q(state),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
